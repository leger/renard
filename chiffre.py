#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2017, Jean-Benoist Leger <jb@leger.tf>
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
# 
#     Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in
#     the documentation and/or other materials provided with the
#     distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


class nb:
    def __init__(self,a,b=None,op=None):
        if type(a) is type(self):
            self.a = a
            self.b = b
            self.op = op
            self.valid = True
            self.simple = False
            if op == '+':
                self.value = a.value + b.value
            elif op == '-':
                if a.value <= b.value:
                    self.valid = False
                else:
                    self.value = a.value - b.value
            elif op == '*':
                self.value = a.value * b.value
            elif op == '/':
                if a.value % b.value == 0:
                    self.value = a.value // b.value
                else:
                    self.valid = False
            else:
                self.valid = False
        else:
            self.value = a
            self.valid = True
            self.simple = True

    def tostr(self, extpth = False):
        if self.valid:
            if self.simple:
                return "%i" % self.value
            else:
                s = None
                if self.op == '+':
                    s = "%s+%s" % (self.a.tostr(), self.b.tostr())
                if self.op == '-':
                    s = "%s-%s" % (self.a.tostr(), self.b.tostr(True))
                if self.op == '*' or self.op == '/':
                    s = "%s%s%s" % (self.a.tostr(True),self.op,self.b.tostr(True))
                if s is not None:
                    if extpth:
                        return "(%s)" % s
                    return "%s" % s
                        

    def __eq__(self, oth):
        if self.simple:
            if not oth.simple:
                return False
            return self.value == oth.value
        if oth.simple:
            return False
        if self.op != oth.op:
            return False
        if self.a == oth.a and self.b == oth.b:
            return True
        if self.op in ('+','*'):
            if self.a == oth.b and self.b == oth.a:
                return True
        return False


def dropdup(pos):
    pos2 = [pos[0]]
    for i in range(1,len(pos)):
        insert = True
        for k in range(len(pos2)):
            if pos2[k] == pos[i]:
                insert = False
        if insert:
            pos2.append(pos[i])
    return pos2



def oneop(v):
    res = []
    for i in range(len(v)):
        for j in range(len(v)):
            if i!=j:
                for op in ('+','-','*','/'):
                    new = nb(v[i], v[j], op)
                    if new.valid:
                        v2 = [v[k] for k in range(len(v)) if k!=i and k!=j]
                        v2.append(new)
                        res.append(v2)
    return res

import numpy as np
def genreduce(v,limit = 100):
    pos = [v]
    allcombi = []
    while len(pos[0])>1:
        pos2 = []
        for p in pos:
            pos2.extend(oneop(p))
        pos = dropdup(pos2)
        for p in pos:
            allcombi.extend(p)
        if len(pos)>limit:
            x = np.random.uniform(size=len(pos))
            mask = x<np.sort(x)[limit]
            pos = [pos[i] for i in range(len(pos)) if mask[i]]
    return [a for a in allcombi if a.value>100 and a.value<=1000]


def gentirage():
    urne = []
    for i in range(1,11):
        urne.append(i)
        urne.append(i)
    for i in (25,50,75,100):
        urne.append(i)

    x = np.random.uniform(size=len(urne))
    mask = x<np.sort(x)[6]
    return [urne[i] for i in range(len(urne)) if mask[i]]

import random
def genjeu(hardness=2):
    while True:
        tirage = gentirage()
        combis = genreduce([nb(t) for t in tirage])
        if len(combis)>0:
            break
    u = np.unique([h.value for h in combis])
    c = [np.sum([h.value == x for h in combis]) for x in u]
    combis = [h for h in combis if h.value in u[c<=np.min(c)+hardness]]
    c = random.choice(combis)
    return (tirage, c.value, c.tostr())

import ast

def calc_str(expr):
    a = ast.parse(expr, mode='eval').body
    value = evaluate(a)
    used = findused(a)
    return(value, used)

def evaluate(n):
    if isinstance(n, ast.Num): # <number>
        if isinstance(n.n,int):
            return n.n
        raise SyntaxError
    elif isinstance(n, ast.BinOp):
        if isinstance(n.op, ast.Add):
            return evaluate(n.left) + evaluate(n.right)
        if isinstance(n.op, ast.Sub):
            left = evaluate(n.left)
            right = evaluate(n.right)
            return left - right
        if isinstance(n.op, ast.Mult):
            return evaluate(n.left)*evaluate(n.right)
        if isinstance(n.op, ast.Div):
            left = evaluate(n.left)
            right = evaluate(n.right)
            if left % right != 0:
                raise SyntaxError
            return left // right
    raise SyntaxError

def findused(n):
    if isinstance(n, ast.Num):
        return [n.n]
    used = []
    used.extend(findused(n.left))
    used.extend(findused(n.right))
    return used

def isvalidgame(tirage, valeur, reponse):
    try:
        propo, used = calc_str(reponse)
    except SyntaxError:
        return (False,None)
    if propo != valeur:
        return (False,'val')
    reste = tirage[:]
    for k in used:
        if k in reste:
            reste.pop(reste.index(k))
        else:
            return (False,'nbs')
    return (True,'')

